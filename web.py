from flask import Flask
from flask import render_template
from flask import request
import sqlite3

app = Flask(__name__)

# Web interface example (root page).
@app.route("/")
def home():
    return "This is my first page on Flask"

# Web interface to show statistics.
@app.route("/stats")
def stats():
    rows = db_query("select * from stats")
    return render_template("stats.html", data=rows)

# Web interface to show all students.
@app.route("/students")
def students():
    rows = db_query("select * from students")
    return render_template("students.html", data=rows)

# Web interface to insert a student record.
@app.route("/students/insert", methods=['POST', 'GET'])
def insert_student():
    g = request.form['gender']
    h = request.form['height']
    w = request.form['weight']
    p = request.form['province']
    n = request.form['noodle']
    db_execute("insert into students values('{0}',{1},{2},'{3}','{4}')".format(
        g, h, w, p, n
    ))
    return "ok"

# Entry page for user registration
@app.route("/register")
def register():
    departments = db_query("select * from depts")
    return render_template("register.html", drows=departments)

@app.route("/adduser", methods=['POST'])
def add_user():
    uname = request.form['uname']
    upass = request.form['upass']
    upass2 = request.form['upass2']
    full_name = request.form['fullName']
    dept_id = request.form['deptID']
    db_execute("insert into users values ('{0}', '{1}', '{2}', '{3}') ".format(
        uname, full_name, upass, dept_id
    ))
    return render_template("info.html")

# Function to write data to database.
def db_execute(sql):
    '''
    This function executes a SQL statement
    to insert, update, or delete.
    :param sql: an insert/update/delete SQL statement.
    :return: none
    '''
    conn = sqlite3.connect("lanzhou.db")
    c = conn.cursor()
    c.execute(sql)
    conn.commit()

# Function to query data.
def db_query(sql):
    '''
    This function retrieves data from database.
    :param sql: the select statement in SQL
    :return: the retrieved data based on the SQL
    '''
    conn = sqlite3.connect("lanzhou.db")
    c = conn.cursor()
    c.execute(sql)
    return c.fetchall()
