
## Install Flask

Run the following command line with pip (or pip3):

```shell
pip install -U Flask
```

pip should be part of python installation.

### If you don't have pip

In case you do not have pip on windows, this link might help you install it:
http://timmyreilly.azurewebsites.net/python-flask-windows-development-environment-setup/


## Create a simple Flask app

Example python code in web.py:

```python
from flask import Flask

app = Flask(__name__)

@app.route("/")
def home():
    return "My first page on Flask..."

@app.route("/sayhello")
def hello():
    return "hello world!"
```


## Run the app

Run the following command line:

```shell
FLASK_APP=web.py flask run
```

Or in the debug/development mode:

```shell
FLASK_APP=web.py FLASK_ENV=development flask run
```

### If you are on windows

The command might be:

```shell
set FLASK_APP=web.py
set FLASK_ENV=development
flask run
```


## Templates

Create a "templates" folder and add a .html files.

Then load the template in python code, such as:

```python
for functions in web.py:
    return render_template("FileName.html")
```


## Templates with data

Assume data have been stored in the "data" variable:

```python
    return render_template("FileName.html", data=data)
```

then in the related .html template:

```
{% for row in data %}
       {{ row["ColumnName"] }}
{% endfor %}
```

This can certainly be mixed with html code. 

For more details on Flask templates, see: 
http://flask.pocoo.org/docs/1.0/tutorial/templates/
