import sqlite3

# Establish a connection to database
conn = sqlite3.connect("lanzhou.db")
conn.row_factory = sqlite3.Row
c = conn.cursor()

# create a new table to store the statistics
c.execute("CREATE TABLE IF NOT EXISTS stats ("
          "cat varchar(10), choice varchar(20), "
          "noodle varchar(5), freq int"
          ")")
c.execute("delete from stats")

# run statistics on gender ~ noodle
c.execute("select lower(gender) as g, lower(noodle) as n, count(*) as c "
          "from students group by lower(gender), lower(noodle)")
rows = c.fetchall()

for row in rows:
    print("{0}, {1}, {2}".format(row['g'], row['n'], row['c']))
    c.execute("insert into stats (cat, choice, noodle, freq) "
              "values('gender', '{0}', '{1}', {2})".format(
        row['g'], row['n'], row['c']
    ))

# run statistics on province ~ noodle
c.execute("select lower(province) as p, lower(noodle) as n, count(*) as c "
          "from students group by lower(province), lower(noodle)")
rows = c.fetchall()
for row in rows:
    print("{0}, {1}, {2}".format(row['p'], row['n'], row['c']))
    c.execute("insert into stats (cat, choice, noodle, freq) "
              "values('province', '{0}', '{1}', {2})".format(
        row['p'], row['n'], row['c']
    ))

# Commit and close database
conn.commit()
c.close()

