import sqlite3

# establish a connection to db
conn = sqlite3.connect("lanzhou.db")
c = conn.cursor()

# create a new table to store results
c.execute("create table if not exists results ("
          " cat varchar(10), choice varchar(20), "
          " noodle varchar(5), freq int"
          ")")
c.execute("delete from results")

# run SQL statements
# retrieve data from students tables
c.execute("select lower(gender), lower(noodle), count(*) from students "
          " group by lower(gender), lower(noodle)")
rows = c.fetchall()
for row in rows:
    print("{0}, {1}, {2}".format(row[0],row[1], row[2]))
    c.execute("insert into results (cat, choice, noodle, freq) "
              " values('gender', '{0}', '{1}', {2})".format(row[0],row[1], row[2]))

# run SQL statements
# retrieve data from students tables
c.execute("select lower(province), lower(noodle), count(*) from students "
          " group by lower(province), lower(noodle)")
rows = c.fetchall()
for row in rows:
    print("{0}, {1}, {2}".format(row[0],row[1], row[2]))
    c.execute("insert into results (cat, choice, noodle, freq) "
              " values('province', '{0}', '{1}', {2})".format(row[0],row[1], row[2]))




# commit and close connection
conn.commit()
conn.close()