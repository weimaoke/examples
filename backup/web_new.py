from flask import Flask
from flask import render_template
from flask import request
# from flask import send_from_directory
import sqlite3

app = Flask(__name__)

# Entry page of the web app
@app.route("/")
def home():
    return "My name is Weimao Ke"

# Students list
@app.route("/students")
def students():
    rows = db_query("select upper(gender), height, weight, "
                    "upper(province), upper(noodle) from students")
    return render_template("students.html", data=rows)

# Retrieve statistics
@app.route("/stats")
def stats():
    rows = db_query("select * from stats")
    return render_template("stats.html", data=rows)

# Insert a new student record
@app.route("/students/insert", methods=['POST','GET'])
def insert_student():
    g = request.form["gender"]
    h = request.form["height"]
    w = request.form["weight"]
    p = request.form["province"]
    n = request.form["noodle"]
    db_execute("insert into students values ('{0}', {1}, {2}, '{3}', '{4}')".format(
        g, h, w, p, n
    ))
    return students()

# To delete a student record (to be completed)
# Example URL: http://domainname.com/students/delete/10
@app.route("/students/delete/<int:id>", methods=['GET'])
def delete_student(id):
    '''
    This is to delete a record in the students table.
    Table students needs to have an id column (and primary key).
    :param id: the student's id (primary key)
    :return: back to the students list after done
    '''
    db_execute("delete from students where id={0}".format(id))
    return students()

# Entry page for user registration
@app.route("/register")
def register():
    departments = db_query("select * from depts")
    return render_template("register.html", drows=departments)

# To add a new user account to database
@app.route("/adduser", methods=['POST'])
def add_user():
    uname = request.form['uname']
    upass = request.form['upass']
    upass2 = request.form['upass2']
    full_name = request.form['fullName']
    dept_id = request.form['deptID']
    if upass==upass2:
        try:
            db_execute("insert into users values('{0}','{1}','{2}','{3}')".format(
                uname, full_name, upass, dept_id
            ))
            return render_template("info.html")
        except:
            return render_template("error.html", message="Database error.")
    else:
        return render_template("error.html", message="Passwords do not match.")

    return render_template("info.html")


# To execute an SQL select query
def db_query(sql):
    '''
    To execute an SQL query and get the results.
    :param sql: the select query
    :return: the retrieved data
    '''
    conn = sqlite3.connect("lanzhou.db")
    c = conn.cursor()
    # retrieve data from database
    c.execute(sql)
    rows = c.fetchall()
    conn.commit()
    conn.close()
    return rows

# To execute an SQL statement
# such as insert, update, and delete
def db_execute(sql):
    '''
    To execute any SQL statement, especially
    to insert, update, and delete records.
    :param sql: the SQL statement
    :return: none
    '''
    conn = sqlite3.connect("lanzhou.db")
    c = conn.cursor()
    # retrieve data from database
    c.execute(sql)
    rows = c.fetchall()
    conn.commit()
    conn.close()
