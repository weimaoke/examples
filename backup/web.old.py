from flask import Flask
from flask import render_template
import sqlite3

app = Flask(__name__)
# app.config['TESTING'] = True

@app.route("/")
def home():
    conn = sqlite3.connect('lanzhou.db')
    conn.row_factory = sqlite3.Row
    c = conn.cursor()

    c.execute('select * from students')
    rows = c.fetchall()

    # data = ""
    # for row in rows:
    #     data += row['gender'] + "\n"
    # return data
    # return "My first page on Flask"
    return render_template("index.html", data=rows)


@app.route("/sayhello")
def hello():
    return "hello world!"