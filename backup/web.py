from flask import Flask
from flask import render_template
import sqlite3

app = Flask(__name__)


@app.route("/")
def home():
    return "This is my first page on Flask"

@app.route("/students")
def students():
    conn = sqlite3.connect("lanzhou.db")
    c = conn.cursor()
    c.execute("select * from students")
    rows = c.fetchall()
    conn.commit()
    # c.close()
    return render_template("students.html", data=rows)
